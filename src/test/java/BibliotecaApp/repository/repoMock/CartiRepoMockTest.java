package BibliotecaApp.repository.repoMock;

import BibliotecaApp.model.Carte;
import BibliotecaApp.model.repo.CartiRepo;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class CartiRepoMockTest {
    CartiRepoMock cartiRepoMock;
    List<Carte> carti;
    Carte carte1,carte2;

    @Before
    public void setUp() throws Exception {

        carte1 = new Carte();
        carte1.setTitlu("titlu1");
        carte1.adaugaAutor("autor1");
        carte1.setAnAparitie("2000");
        carte1.setEditura("editura1");
        carte1.setCuvinteCheie(new ArrayList<String>(Arrays.asList("poem","poezie")));


        carte2 = new Carte();
        carte2.setTitlu("titlu2");
        carte2.adaugaAutor("ator2");
        carte2.setAnAparitie("2000");
        carte2.setEditura("editura2");
        carte2.setCuvinteCheie(new ArrayList<String>(Arrays.asList("poem","poezie")));

            cartiRepoMock = new CartiRepoMock();
        }



    @Test
    public void cautaCarteTC1() {
        //TC1  carti.size=0
        //     autor:autor1
        //     expected: []
        //               [ cartiRepoMock.cautaCarte("autor1").size()==0]

        cartiRepoMock= new CartiRepoMock();
        System.out.println("Lista carti:"+cartiRepoMock.getCarti());

        assertEquals("Repository gol confirmat", 0, cartiRepoMock.cautaCarte("autor1").size());
    }

    @Test
    public void cautaCarteTC2() {
        //TC2  carti.size=1
        //     autor: " "
        //    expected:[]  [ cartiRepoMock.cautaCarte("autor1").size()==0]
        cartiRepoMock= new CartiRepoMock();


        cartiRepoMock.adaugaCarte(carte1);
        System.out.println("carti.size :"+cartiRepoMock.getCarti());

        assertEquals(cartiRepoMock.cautaCarte(" ").isEmpty(),true);
    }
@Test
   public void cautaCarteTC3() {
        //TC2  carti.size=1
        //     autor : autor
        //  expected :cartiRepoMock.cautaCarte("autor1").size()>0
        cartiRepoMock= new CartiRepoMock();

        cartiRepoMock.adaugaCarte(carte1);

        System.out.println("carti.size: "+cartiRepoMock.getCarti().size());

        assertEquals(true,cartiRepoMock.cautaCarte("autor1").size()>0);

   }

    @After
    public void tearDown() throws Exception {
        carte1=null;
        carte2=null;
        cartiRepoMock=null;
    }


}